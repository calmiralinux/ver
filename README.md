# ver

`ver` - a simple program to get information about the system, written in Rust.

## Build & install

```bash
git clone https://gitlab.com/calmiralinux/ver
cd ver

cargo build --release
cp -v target/release/ver /usr/bin/ver
```

## Usage

```bash
ver
```
