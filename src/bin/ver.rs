use cursive::views::ListView;
use cursive::views::OnEventView;
use cursive::views::TextView;
use cursive::views::SelectView;
//use cursive::views::LinearLayout;
use cursive::views::Dialog;
use cursive::views::Panel;

use cursive::menu;
use cursive::event::Key;
use cursive::event::EventResult;

use cursive::Cursive;
use cursive::align::HAlign;
use ver::config;

fn main() {
    let mut screen = cursive::default();
    menubar(&mut screen);
    main_win(&mut screen);
    screen.run();
}

/* TODO: add this
fn donat_win(scr: &mut Cursive) {
    let don_txt = "\
        The Calmira GNU/Linux-libre project is developed exclusively by enthusiasts. \
        None of our team receives a penny for the development of the distribution and \
        its components. However, we need to pay our internet and electricity bills, \
        service our computers and laptops, and upgrade them whenever possible. To help \
        us in this, you can send a donation, absolutely any amount! It also motivates us \
        to keep working. Below is a list of people working on Calmira GNU/Linux-libre \
        that you can donate to. Thanks in advance!";

    let devlist = ListView::new()
        .child("Michail Krasnov - Calmira GNU/Linux-libre creator", TextView::new("<XXX>"))
        .child("Sergey Gaberer  - Creator of many system components", TextView::new("<XXX>"));

    let don_win = LinearLayout::vertical()
        .child(TextView::new(don_txt))
        .child(Panel::new(devlist));

    scr.add_layer(
        Dialog::around(don_win)
            .title("Donations")
            .button("Ok", main_win),
    );
}
*/
fn menubar(scr: &mut Cursive) {
    scr.menubar()
        .add_subtree("Help", menu::Tree::new()
            .leaf("About ver", |s| {
                s.add_layer(
                    Dialog::around(TextView::new("ver v1.0.0"))
                        .title("About program")
                        .button("Ok", main_win),
                );
            })
           // .leaf("Where's my money Lebowski?", donat_win)
        );
    scr.set_autohide_menu(false);
    scr.add_global_callback(Key::Esc, |s| s.select_menubar());
}

pub fn main_win(scr: &mut Cursive) {
    scr.pop_layer();

    let mut select = SelectView::new()
        .h_align(HAlign::Left)
        .autojump();
    // Sets the callback for when "Enter" is pressed
    select.set_on_submit(on_selected_main);

    select.add_item("About Calmira GNU/Linux-libre", 1);
    select.add_item("About maintainers", 2);
    select.add_item("About system developers", 3);

    // Let's override the 'j' and 'k' keys for navigation
    let select = OnEventView::new(select)
        .on_pre_event_inner('k', |s, _| {
            let cb = s.select_up(1);
            Some(EventResult::Consumed(Some(cb)))
        })
        .on_pre_event_inner('j', |s, _| {
            let cb = s.select_down(1);
            Some(EventResult::Consumed(Some(cb)))
        });

    let win = Dialog::new()
        .title("About Calmira GNU/Linux-libre")
        .content(Panel::new(select))
        .button("Quit", |s| s.quit());

    scr.add_layer(win);
}

fn on_selected_main(scr: &mut Cursive, selected: &i32) {
    scr.pop_layer();

    let config = match config::CalmRelease::parse() {
        Ok(c) => c,
        Err(why) => {
            let win = Dialog::around(TextView::new(format!("Error while parsing '/etc/calm-release' file: {why}")))
                .title("Critical error")
                .button("Exit", |s| s.quit());
            scr.add_layer(win);
            std::process::exit(1);
        }
    };

    match selected {
        1 => about_system_win(scr, &config.system),
        2 => about_maintainers_win(scr, &config.maintainer),
        3 => about_devs_win(scr, &config.resources),
        _ => main_win(scr),
    }
}

fn about_system_win(scr: &mut Cursive, sys: &config::SystemSection) {
    let content = ListView::new()
        .child("System version:", TextView::new(&sys.version))
        .child("System codename:", TextView::new(&sys.codename))
        .child("Architecture:", TextView::new(&sys.arch))
        .child("Edition:", TextView::new(&sys.edition))
        .child("Build date:", TextView::new(&sys.build_date));

    let win = Dialog::around(Panel::new(content))
        .title("About Calmira GNU/Linux-libre")
        .button("Ok", main_win)
        .button("QUIT", |s| s.quit());

    scr.add_layer(win);
}

fn about_maintainers_win(scr: &mut Cursive, maint: &config::MaintainerSection) {
    let content = ListView::new()
        .child("Builder name:", TextView::new(&maint.name))
        .child("E-mail:", TextView::new(&maint.email))
        .child("Build years:", TextView::new(&maint.years));

    let win = Dialog::around(Panel::new(content))
        .title("About system maintainers")
        .button("Ok", main_win)
        .button("QUIT", |s| s.quit());

    scr.add_layer(win);
}

fn about_devs_win(scr: &mut Cursive, dev: &config::ResourcesSection) {
    let content = ListView::new()
        .child("Organization:", TextView::new(&dev.organization))
        .child("Releases page:", TextView::new(&dev.releases))
        .child("Master repository:", TextView::new(&dev.master_repo));

    let win = Dialog::around(Panel::new(content))
        .title("About development")
        .button("Ok", main_win)
        .button("QUIT", |s| s.quit());

    scr.add_layer(win);
}
