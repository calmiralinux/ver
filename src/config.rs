/*!
Модуль, который парсит конфигурационный файл Calmira GNU/Linux-libre
*/

use serde::Deserialize;
use crate::consts;
use anyhow::Result;
use std::fs;
use toml;

#[derive(Deserialize, Debug)]
pub struct CalmRelease {
    pub system: SystemSection,
    pub maintainer: MaintainerSection,
    pub resources: ResourcesSection,
}

#[derive(Deserialize, Debug)]
pub struct SystemSection {
    pub name: String,
    pub version: String,
    pub codename: String,
    pub description: String,
    pub arch: String,
    pub build: String,
    pub build_date: String,
    pub edition: String,
}

#[derive(Deserialize, Debug)]
pub struct MaintainerSection {
    pub name: String,
    pub email: String,
    pub years: String,
}

#[derive(Deserialize, Debug)]
pub struct ResourcesSection {
    pub organization: String,
    pub master_repo: String,
    pub releases: String,
    pub issues: String,
}

impl CalmRelease {
    pub fn parse() -> Result<Self> {
        let conf_content: CalmRelease = toml::from_str(&fs::read_to_string(consts::CALM_RELEASE_FILE)?)?;
        Ok(conf_content)
    }
}
